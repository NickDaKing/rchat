package de.nickdaking.rchat;

/**
 *
 * @author NickDaKing
 * @version 10.11.2013
 *
 */
public enum CommandInfo {

    PAIR("/rchat pair", "rchat.pair", RChat.messagesConfig.getString("cinfo_pair"), 1, 1),
    UNPAIR("/rchat unpair", "rchat.unpair", RChat.messagesConfig.getString("cinfo_unpair"), 1, 1),
    DISABLE("/rchat disable", "rchat.disable", RChat.messagesConfig.getString("cinfo_disable"), 1, 1),
    ENABLE("/rchat enable", "rchat.enable", RChat.messagesConfig.getString("cinfo_enable"), 1, 1),
    MUTE("/rchat mute <Target>", "rchat.mute", RChat.messagesConfig.getString("cinfo_mute"), 1, 2), 
    UNMUTE("/rchat unmute <Target>", "rchat.unmute", RChat.messagesConfig.getString("cinfo_unmute"), 1, 2);

    private final String cmd;
    private final int minArgs;
    private final int maxArgs;
    private final String perm;
    private final String desc;

    CommandInfo(String cmd, String perm, String desc, int minArgs, int maxArgs) {
        this.cmd = cmd;
        this.minArgs = minArgs;
        this.maxArgs = maxArgs;
        this.perm = perm;
        this.desc = desc;
    }

    /**
     * @return the cmd
     */
    public String getCommand() {
        return cmd;
    }

    /**
     * @return the minArgs
     */
    public int getMinArgs() {
        return minArgs;
    }

    /**
     * @return the maxArgs
     */
    public int getMaxArgs() {
        return maxArgs;
    }

    /**
     * @return the perm
     */
    public String getPermission() {
        return perm;
    }

    /**
     * @return the desc
     */
    public String getDescription() {
        return desc;
    }
}
