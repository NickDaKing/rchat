/*
 * Copyright (C) 2013-2014 NickDaKing
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.nickdaking.rchat;

import de.nickdaking.rchat.message.MessageHandler;
import de.nickdaking.rchat.server.RChatServer;
import de.nickdaking.rchat.utils.YAMLConfigHandler;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class RChat extends JavaPlugin {

    private static final Logger logger = Logger.getGlobal();
    public static FileConfiguration messagesConfig;
    public static Boolean rChatEnabled = true;
    private FileConfiguration settingsConfig;
    public MessageHandler messageHandler;
    public YAMLConfigHandler ymlCfgPaircode;
    public YAMLConfigHandler ymlCfgGMute;
    public static final String prefix = "[rChat] ";
    private RChatServer server;

    @Override
    public void onEnable() {
        loadConfig();
        loadMessagesConfig();

        ymlCfgPaircode = new YAMLConfigHandler(this, "paircodes.yml");
        ymlCfgPaircode.reloadConfig();

        ymlCfgGMute = new YAMLConfigHandler(this, "gmute.yml");
        ymlCfgGMute.reloadConfig();

        server = new RChatServer(settingsConfig.getInt("server-port"), this, settingsConfig.getBoolean("debug"));
        getServer().getPluginManager().registerEvents(server, this);

        messageHandler = new MessageHandler(messagesConfig, prefix);

        getCommand("rchat").setExecutor(new RChatCommandExecutor(messageHandler, ymlCfgPaircode, ymlCfgGMute));

        logger.log(Level.INFO, "{0}rChat has been enabled", prefix);
    }

    @Override
    public void onDisable() {
        logger.log(Level.INFO, "{0}rChat has been disabled", prefix);
    }

    private void loadConfig() {
        reloadConfig();
        File configFile = new File(getDataFolder(), "config.yml");

        if (!configFile.exists()) {
            saveDefaultConfig();
            saveConfig();
        }
        settingsConfig = getConfig();
        settingsConfig.options().copyDefaults(true);
        saveConfig();

        reloadConfig();
    }

    private void loadMessagesConfig() {
        YAMLConfigHandler ymlCfgMessages = new YAMLConfigHandler(this, "messages_" + settingsConfig.getString("language") + ".yml");
        ymlCfgMessages.reloadConfig();

        ymlCfgMessages.saveDefaultConfig();
        messagesConfig = ymlCfgMessages.getConfig();
        messagesConfig.options().copyDefaults(true);

        ymlCfgMessages.saveConfig();
    }
}
