package de.nickdaking.rchat.server;


/**
 *
 * @author NickDaKing
 */
public class ConnectionLostTimer implements Runnable {
    
    private final RChatServerThread connectionThread;
    private final Boolean debug;
    
    public ConnectionLostTimer(RChatServerThread connectionThread, boolean debug) {
        this.connectionThread = connectionThread;
        this.debug = debug;
    }

    @Override
    public void run() {
        if (debug) {
            System.out.println("DEBUG: DISCONNECT TIMEOUT");
        }
        connectionThread.send("BYE BYE");
        connectionThread.destroy();
    }

}
