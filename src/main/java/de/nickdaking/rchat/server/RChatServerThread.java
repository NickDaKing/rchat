package de.nickdaking.rchat.server;

import de.nickdaking.rchat.RChat;
import static de.nickdaking.rchat.RChat.rChatEnabled;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

/**
 *
 * @author NickDaKing
 */
public class RChatServerThread extends Thread {

    private final long TIMEOUT = 100; //5 sec
    private final long MESSAGE_THROTTLE = 1000; //1 sec

    private final RChat plugin;
    private final Socket socket;
    private final RChatServer server;
    private BufferedReader input;
    private BufferedWriter output;

    private BukkitTask bukkitTask;
    private final Runnable keepAliveTask;

    private Boolean authenticated = false;
    private String player;
    private Date lastChat = new Date();
    private final Boolean debug;

    public RChatServerThread(RChatServer server, Socket socket, RChat plugin, boolean debug) throws SocketException {
        this.server = server;
        this.socket = socket;
        this.plugin = plugin;
        this.debug = debug;
        this.keepAliveTask = new ConnectionLostTimer(this, debug);
        bukkitTask = Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, keepAliveTask, TIMEOUT);
    }

    @Override
    public void run() {
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            String clientMsg;
            send("Welcome to rChatv1");
            while ((clientMsg = input.readLine()) != null) {
                String[] args = clientMsg.split(":");
                if (!authenticated) {
                    if (args[0].equals("connect")) {
                        if (args.length != 2) {
                            send("+err {protocol error}");
                            destroy();
                            continue;
                        }
                        if (authenticate(args[1])) {
                            server.broadcast(RChat.prefix + player + " joined the chat");
                            plugin.messageHandler.sendMessageText(plugin.getServer().getOnlinePlayers(), "join", new String[]{player}, ChatColor.YELLOW);
                            continue;
                        }
                    }
                }
                if (debug) {
                    System.out.println("DEBUG: Command " + args[0] + " from " + socket.getInetAddress().getHostAddress());
                }
                switch (args[0]) {
                    case "connect":
                        send("+err {already connected}");
                        break;
                    case "chat":
                        if (args.length != 2) {
                            send("+err {protocol error}");
                            break;
                        }
                        chat((String) args[1]);
                        break;
                    case "getonlineplayers":
                        sendOnlinePlayers();
                        break;
                    case "disconnect":
                        this.destroy();
                        break;
                    case "ping":
                        bukkitTask.cancel();
                        bukkitTask = Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, keepAliveTask, TIMEOUT);
                        send("pong");
                        break;
                    default:
                        send("+err {protocol error}");
                        break;
                }
            }
        } catch (IOException ex) {
            destroy();
        }
    }

    public void send(String message) {
        try {
            if (!socket.isClosed()) {
                output.write(message);
                output.newLine();
                output.flush();
            }
        } catch (IOException ex) {
            Logger.getLogger(RChatServerThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void chat(String message) {
        if ((new Date().getTime() - lastChat.getTime()) < MESSAGE_THROTTLE) {
            send("+err {too much messages}");
            return;
        }
        lastChat = new Date();
        if (!rChatEnabled) {
            send("+err {chat is down}");
            return;
        }
        if (plugin.ymlCfgGMute.getConfig().getBoolean(player)) {
            send("+err {you have been muted}");
            return;
        }
        server.broadcast("<" + player + "> " + Base64Coder.decodeString(message));
        Bukkit.getServer().broadcastMessage("<" + player + "> " + Base64Coder.decodeString(message));
        send("+ok {chat}");
    }

    public void sendOnlinePlayers() {
        StringBuilder players = new StringBuilder();
        for (Player p : plugin.getServer().getOnlinePlayers()) {
            players.append(":").append(p.getName());
        }
        players.append("#");
        send(players.insert(0, "onlineplayers").toString());
    }

    private Boolean authenticate(String paircode) throws IOException {
        player = plugin.ymlCfgPaircode.getConfig().getString(paircode);
        authenticated = player != null;
        if (authenticated) {
            send("+ok {auth}");
        } else {
            send("+err {auth}");
            destroy();
        }
        return authenticated;
    }

    @Override
    public void destroy() {
        try {
            send("BYE BYE");
            bukkitTask.cancel();
            input.close();
            output.close();
            socket.close();
            if (player != null) {
                plugin.messageHandler.sendMessageText(plugin.getServer().getOnlinePlayers(), "left", new String[]{player}, ChatColor.YELLOW);
            }
            server.removeFromList(this);
            stop();
        } catch (IOException ex) {
            Logger.getLogger(RChatServerThread.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
    }

    public Boolean isAuthenticated() {
        return authenticated;
    }

    public Socket getSocketConnection() {
        return socket;
    }
}
