package de.nickdaking.rchat.server;

import de.nickdaking.rchat.RChat;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

/**
 *
 * @author NickDaKing
 */
public final class RChatServer implements Listener {

    private ServerSocket serverSocket;
    private Boolean shouldHalt = false;
    private RChat plugin;
    private final RChatServer server = this;
    private final ArrayList<RChatServerThread> openconnections = new ArrayList<>();
    private Boolean debug;

    public RChatServer(int serverPort, RChat plugin, boolean debug) {
        try {
            this.plugin = plugin;
            this.debug = debug;
            serverSocket = new ServerSocket(serverPort);
            this.serverTask();
        } catch (IOException ex) {
            Logger.getLogger(RChatServer.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
    }

    public void stop() throws IOException {
        shouldHalt = true;
        broadcast("<rChat> Shutdown");
        for (RChatServerThread t : openconnections) {
            t.destroy();
        }
        serverSocket.close();
    }

    public void serverTask() {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {

            @Override
            public void run() {
                Socket socket;
                while (!shouldHalt) {
                    try {
                        socket = serverSocket.accept();
                        RChatServerThread thread = new RChatServerThread(server, socket, plugin, debug);
                        openconnections.add(thread);
                        if (debug) {
                            System.out.println("DEBUG: New Connection from " + socket.getInetAddress().getHostAddress());
                            System.out.println("DEBUG: Open connections: " + openconnections.size());
                        }
                        thread.start();
                    } catch (IOException ex) {
                        Logger.getLogger(RChatServer.class.getName()).log(Level.SEVERE, ex.getMessage());
                    }
                }
            }
        });
    }
    
    public Boolean removeFromList(RChatServerThread t) {
        return openconnections.remove(t);
    }

    public void broadcast(final String message) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {

            @Override
            public void run() {
                for (RChatServerThread t : openconnections) {
                    if (t == null || !t.isAlive()) {
                        continue;
                    }
                    if (t.getSocketConnection().isClosed() || !t.isAuthenticated()) {
                        continue;
                    }
                    t.send("msg:" + Base64Coder.encodeString(message));
                }
            }
        });
    }

/*    @EventHandler
    public void onPlayerHeroChat(ChannelChatEvent event) {
        if (event.getChannel().isLocal()) {
            return;
        }
        if (event.getChannel().isHidden()) {
            return;
        }
        broadcast(event.getMessage());
    }
*/
    
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        broadcast("<" + event.getPlayer().getDisplayName() + "> " + event.getMessage());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        broadcast(event.getJoinMessage().substring(2));
    }
    
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        broadcast(event.getQuitMessage().substring(2));
    }
}
