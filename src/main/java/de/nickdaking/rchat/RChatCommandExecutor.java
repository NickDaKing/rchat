package de.nickdaking.rchat;

import de.nickdaking.rchat.message.MessageHandler;
import de.nickdaking.rchat.message.MessagePool;
import de.nickdaking.rchat.utils.YAMLConfigHandler;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author NickDaKing
 * @version 10.11.2013
 *
 */
public class RChatCommandExecutor implements CommandExecutor {

    private final MessageHandler messageHandler;
    private final YAMLConfigHandler ymlCfgPaircode;
    private final YAMLConfigHandler ymlCfgGMute;
    private final static Logger logger = Logger.getLogger(RChatCommandExecutor.class.getName());
    private final SecureRandom random = new SecureRandom();

    public RChatCommandExecutor(MessageHandler messageHandler, YAMLConfigHandler ymlCfgPaircode, YAMLConfigHandler ymlCfgGMute) {
        this.messageHandler = messageHandler;
        this.ymlCfgPaircode = ymlCfgPaircode;
        this.ymlCfgGMute = ymlCfgGMute;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lable, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }
        Player player = (Player) sender;
        if (cmd.getName().equalsIgnoreCase("rchat")) {
            CommandInfo ci;
            try {
                ci = CommandInfo.valueOf(args[0].toUpperCase());
            } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException ex) {
                messageHandler.sendMessageFromConfig(player, MessagePool.UNKNOWN_COMMAND, null, ChatColor.RED);
                return true;
            }
            if (!(player.hasPermission(ci.getPermission()) || player.isOp())) {
                messageHandler.sendMessageFromConfig(player, MessagePool.NO_PERMISSION, null, ChatColor.RED);
            }

            if (args.length < ci.getMinArgs() || args.length > ci.getMaxArgs()) {
                messageHandler.sendMessageFromConfig(player, MessagePool.CORRECT_USAGE, new String[]{ci.getCommand()}, ChatColor.RED);
            }

            switch (ci) {
                case PAIR:
                    pair(player);
                    break;
                case UNPAIR:
                    unpair(player);
                    break;
                case ENABLE:
                    if (RChat.rChatEnabled) {
                        messageHandler.sendMessageFromConfig(player, "alreadyEnabled", null, ChatColor.RED);
                        break;
                    }
                    RChat.rChatEnabled = true;
                    messageHandler.sendMessageFromConfig(player, "enabled", null, ChatColor.YELLOW);
                    break;
                case DISABLE:
                    if (!RChat.rChatEnabled) {
                        messageHandler.sendMessageFromConfig(player, "alreadyDisabled", null, ChatColor.RED);
                        break;
                    }
                    RChat.rChatEnabled = false;
                    messageHandler.sendMessageFromConfig(player, "disabled", null, ChatColor.YELLOW);
                    break;
                case MUTE:
                    if (args.length > 1) {
                        mute(player, args[1]);
                    }
                    break;
                case UNMUTE:
                    if (args.length > 1) {
                        unmute(player, args[1]);
                    }
                    break;
                default:
                    break;
            }
            return true;
        }
        return false;
    }

    private void pair(Player player) {
        for (String paircode : ymlCfgPaircode.getConfig().getKeys(true)) {
            if (ymlCfgPaircode.getConfig().getString(paircode).equals(player.getName())) {
            messageHandler.sendMessageFromConfig(player, "alreadyPaired", null, ChatColor.RED);
            return;
            }
        }
        String paircode = new BigInteger(130, random).toString(32).substring(0, ymlCfgPaircode.getConfig().getInt("paircodelength"));
        ymlCfgPaircode.getConfig().set(paircode, player.getName());
        ymlCfgPaircode.saveConfig();
        messageHandler.sendMessageFromConfig(player, "pairode", new String[]{ChatColor.LIGHT_PURPLE + paircode + ChatColor.YELLOW}, ChatColor.YELLOW);
    }

    private void unpair(Player player) {
        for (String paircode : ymlCfgPaircode.getConfig().getKeys(true)) {
            if (ymlCfgPaircode.getConfig().getString(paircode).equals(player.getName())) {
                ymlCfgPaircode.getConfig().set(paircode, null);
                ymlCfgPaircode.saveConfig();
                messageHandler.sendMessageFromConfig(player, "unpaird", null, ChatColor.YELLOW);
                return;
            }
        }
        messageHandler.sendMessageFromConfig(player, "notPaired", null, ChatColor.RED);
    }

    private void mute(Player player, String target) {
        if (ymlCfgGMute.getConfig().getBoolean(target)) {
            messageHandler.sendMessageFromConfig(player, "alreadyMuted", null, ChatColor.RED);
            return;
        }
        ymlCfgGMute.getConfig().set(target, true);
        ymlCfgGMute.saveConfig();
        messageHandler.sendMessageFromConfig(Bukkit.getServer().getOnlinePlayers(), "mute", new String[]{target}, ChatColor.YELLOW);
    }

    private void unmute(Player player, String target) {
        if (!ymlCfgGMute.getConfig().getBoolean(target)) {
            messageHandler.sendMessageFromConfig(player, "notMuted", null, ChatColor.RED);
            return;
        }
        ymlCfgGMute.getConfig().set(target, null);
        ymlCfgGMute.saveConfig();
        messageHandler.sendMessageFromConfig(Bukkit.getServer().getOnlinePlayers(), "unmute", new String[]{target}, ChatColor.YELLOW);
    }
}
