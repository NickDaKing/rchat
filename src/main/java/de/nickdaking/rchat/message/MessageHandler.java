package de.nickdaking.rchat.message;

import de.nickdaking.rchat.CommandInfo;
import java.util.ArrayList;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 *
 * @author NickDaKing
 * @version 10.11.2013
 *
 */
public class MessageHandler {

    private final FileConfiguration messagesConfig;
    private final String prefix;

    /**
     * Creates a messagehandler. Used to send messages from yml-file to players.
     *
     * @param messagesConfig Fileconfiguration object of messages.yml
     * @param prefix the message prefix. E.g. pluginname.
     */
    public MessageHandler(FileConfiguration messagesConfig, String prefix) {
        this.messagesConfig = messagesConfig;
        this.prefix = prefix;
    }

    /**
     * Sends a message from config to one player.
     *
     * @param player The player who should get the message
     * @param msgPoolID The message id.
     * @param params Parameter. Will replace {n] in message string with strings
     * in params.
     * @param color The color.
     */
    public void sendMessageFromConfig(Player player, String msgPoolID, String[] params, ChatColor color) {
        if (color == null) {
            color = ChatColor.WHITE;
        }
        String msg = ChatColor.GOLD + prefix + color + messagesConfig.getString(msgPoolID);
        if (params != null) {
            for (int i = 0; i < params.length; i++) {
                msg = msg.replace("{" + (i + 1) + "}", params[i]);
            }
        }
        player.sendMessage(msg);
    }

    /**
     * Sends a message from config to more players.
     *
     * @param players Players who should get the message
     * @param msgPoolID The message id.
     * @param params Parameter. Will replace {n] in message string with strings
     * in params.
     * @param color The color.
     */
    public void sendMessageFromConfig(Player[] players, String msgPoolID, String[] params, ChatColor color) {
        for (Player p : players) {
            sendMessageFromConfig(p, msgPoolID, params, color);
        }
    }

    /**
     * Sends a message to one player.
     *
     * @param player The player who should get the message
     * @param text The message.
     * @param params Parameter. Will replace {n] in message string with strings
     * in params.
     * @param color The color.
     */
    public void sendMessageText(Player player, String text, String[] params, ChatColor color) {
        String msg = ChatColor.GOLD + prefix + color + text;
        if (params != null) {
            for (int i = 0; i < params.length; i++) {
                msg = msg.replace("{" + (i + 1) + "}", params[i]);
            }
        }
        player.sendMessage(msg);
    }

    /**
     * Sends a message to more players.
     *
     * @param players The player who should get the message
     * @param text The message.
     * @param params Parameter. Will replace {n] in message string with strings
     * in params.
     * @param color The color.
     */
    public void sendMessageText(Player[] players, String text, String[] params, ChatColor color) {
        for (Player p : players) {
            sendMessageFromConfig(p, text, params, color);
        }
    }

    /**
     * Sends a list of commands to a player. 
     *
     * @param player the player
     * @param sPage page of help list
     */
    public void sendHelp(Player player, String sPage) {
        int page = 1;
        try {
            page = Integer.parseInt(sPage);
        } catch (NumberFormatException e) {
        }
        ArrayList<String> messages = new ArrayList<>();
        for (CommandInfo i : CommandInfo.values()) {
            if (player.hasPermission(i.getPermission())) {
                messages.add(new StringBuilder().append(ChatColor.GOLD).append(i.getCommand()).append(ChatColor.WHITE).append(" - ").append(i.getDescription()).append(".").toString());
            }
        }

        if (page * 15 - 15 > messages.size()) {
            page = 1;
        }

        player.sendMessage(new StringBuilder().append(ChatColor.GOLD).append(prefix).append(ChatColor.WHITE).append("------").append(" Hilfe (Seite ").append(page).append(" von ").append(messages.size() / 15 + 1).append(")").append("------").toString());
        player.sendMessage(messagesConfig.getString("cinfo_plugin"));
        player.sendMessage("Befehl: /rchat");

        for (int i = (page - 1) * 15; i < page * 15 && i < messages.size(); i++) {
            player.sendMessage(messages.get(i));
        }

    }

    /**
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }
}
