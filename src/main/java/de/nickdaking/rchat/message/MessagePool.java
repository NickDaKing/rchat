package de.nickdaking.rchat.message;

import org.bukkit.ChatColor;

/**
 *
 * @author NickDaKing
 * @version 10.11.2013
 * 
 */
public class MessagePool {
    //TEXTS
    public static final String TEXT_MSG_HELP=ChatColor.AQUA+""+ChatColor.BOLD+"BiberCraft Bow\n"+ChatColor.AQUA+"Version 1.0 - entwickelt von goasi und Chrzi\n"+"Für Hilfe mach bitte /bow help";
    
    //TEXT-IDs
    public static final String NO_PERMISSION = "noPermission";
    public static final String UNKNOWN_COMMAND = "unknownCommand";
    public static final String CORRECT_USAGE = "correctUsage";
}
