package de.nickdaking.rchat.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author NickDaKing
 */
public class YAMLConfigHandler {

    private File yamlFile = null;
    private FileConfiguration yamlConfig = null;
    private final JavaPlugin plugin;
    private String filename;

    public YAMLConfigHandler(JavaPlugin plugin, String filename) {
        if (!plugin.isInitialized()) {
            throw new IllegalArgumentException("plugin must be initiaized");
        }
        this.filename = filename;
        this.plugin = plugin;
    }

    public void reloadConfig() {
        if (this.yamlFile == null) {
            this.yamlFile = new File(this.plugin.getDataFolder(), filename);
        }
                
        this.yamlConfig = YamlConfiguration.loadConfiguration(this.yamlFile);
        InputStream defConfigStream = this.plugin.getResource(yamlFile.getName());
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            yamlConfig.setDefaults(defConfig);
        }
    }

    public void saveConfig() {
        if ((this.yamlConfig == null) || (this.yamlFile == null)) {
            return;
        }
        try {
            getConfig().save(this.yamlFile);
        } catch (IOException ex) {
            this.plugin.getLogger().log(Level.SEVERE, "Could not save config to " + this.yamlFile, ex);
        }
    }

    public FileConfiguration getConfig() {
        if (this.yamlConfig == null) {
            reloadConfig();
        }
        return this.yamlConfig;
    }

    public void saveDefaultConfig() {
        if (!this.yamlFile.exists()) {
            this.plugin.saveResource(filename, false);
        }
    }
}
